package fr.jSimDonjon.Player;

public class PlayerThread extends Thread{
    private RunnableSound runnableSound;
    public PlayerThread(RunnableSound runnable) {
        super(runnable);
        this.runnableSound = runnable;
    }

    public void stopPlayer() {
        this.runnableSound.stop();
    }
    
}
