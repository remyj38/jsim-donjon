package fr.jSimDonjon.Player;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javazoom.jl.player.advanced.AdvancedPlayer;

        // MP3, WMA, MPEG, WAV compatible
public class Sound {

    private Boolean isPlaying = false;
    private AdvancedPlayer player = null;

    public Sound(String path) throws Exception {
        InputStream input;
        if (path.equals("Textures/Default/song.mp3")) {
            input = (InputStream) new BufferedInputStream(Sound.class.getResourceAsStream("/" + path));
        } else {
            input = (InputStream) new BufferedInputStream(new FileInputStream(new File(path)));
        }

        player = new AdvancedPlayer(input);
        isPlaying = true;
        player.play();
    }

    public void play() throws Exception {
        if (player != null) {
            isPlaying = true;
            player.play();
        }
    }

    public void stop() throws Exception {
        if (player != null) {
            isPlaying = false;
            player.stop();
        }
    }

    public Boolean isPlaying() {
        return isPlaying;
    }
}
