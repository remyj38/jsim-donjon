package fr.jSimDonjon.Player;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RunnableSound implements Runnable {

    private String path;
    private Sound song;

    public RunnableSound(String path) {
        this.path = path;
    }

    @Override
    public void run() {
        try {
            this.song = new Sound(path);
            this.song.play();
        } catch (Exception ex) {
            Logger.getLogger(RunnableSound.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stop() {
        try {
            this.song.stop();
        } catch (Exception ex) {
            Logger.getLogger(RunnableSound.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
