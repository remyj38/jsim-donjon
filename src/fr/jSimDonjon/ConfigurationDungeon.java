package fr.jSimDonjon;

import fr.jSimDonjon.Configuration.IConfiguration;

public class ConfigurationDungeon implements IConfiguration {

    private int densityRock;
    private int densityWall;
    private int height;
    private int width;
    private int nbCharacters;
    private int refreshTime;
    private Boolean stepByStep;
    private String texturePack;
    private Boolean sound;

    public ConfigurationDungeon() {
        this.densityRock = 20;
        this.densityWall = 10;
        this.height = 100;
        this.width = 100;
        this.nbCharacters = 10;
        this.refreshTime = 100;
        this.stepByStep = false;
        this.texturePack = "Default";
        this.sound = false;
    }
    
    @Override
    public int getDensityRock() {
        return this.densityRock;
    }

    @Override
    public int getDensityWall() {
        return this.densityWall;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getNbCharacters() {
        return this.nbCharacters;
    }

    @Override
    public int getRefreshTime() {
        return this.refreshTime;
    }
    
    @Override
    public Boolean getStepByStep() {
        return this.stepByStep;
    }
    
    

    @Override
    public void setDensityRock(int rock) {
        this.densityRock = rock;
    }

    @Override
    public void setDensityWall(int wall) {
        this.densityWall = wall;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setNbCharacters(int nbCharacters) {
        this.nbCharacters = nbCharacters;
    }

    @Override
    public void setRefreshTime(int refreshTime) {
        this.refreshTime = refreshTime;
    }

    @Override
    public void setStepByStep(Boolean status) {
        this.stepByStep = status;
    }

    @Override
    public String getTexturePack() {
        return this.texturePack;
    }

    @Override
    public void setTexturepack(String texturePack) {
        this.texturePack = texturePack;
    }

    @Override
    public Boolean getSound() {
        return sound;
    }

    @Override
    public void setSound(Boolean sound) {
        this.sound = sound;
    }
    
}
