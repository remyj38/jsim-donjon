package fr.jSimDonjon;

import fr.affichagePlateau.Plateau;
import fr.jSimDonjon.Case.Border;
import fr.jSimDonjon.Case.Case;
import fr.jSimDonjon.Case.CharDead;
import fr.jSimDonjon.Case.Empty;
import fr.jSimDonjon.Case.Rock;
import fr.jSimDonjon.Case.Shine;
import fr.jSimDonjon.Case.Wall;
import fr.jSimDonjon.Configuration.IConfiguration;
import fr.jSimDonjon.Configuration.VisualiseFrame;
import fr.jSimDonjon.Entity.Characters;
import fr.jSimDonjon.Entity.MobilesElements;
import fr.jSimDonjon.Entity.Monster;
import fr.jSimDonjon.Entity.Scoot;
import fr.jSimDonjon.Entity.Weak;
import fr.jSimDonjon.Player.PlayerThread;
import fr.jSimDonjon.Player.RunnableSound;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public final class Dungeon {

    public final IConfiguration configuration;

    private final int nbTypesObstacles = 3;
    private final int nbTypesCharacters = 3;

    private final Random random;
    private final Plateau dungeon;
    private final Case cases[][];
    private final ArrayList<MobilesElements> movable;
    private int xMonster = -1;
    private int yMonster = -1;
    private int scootShow = -1;
    public Boolean stepStatus = false;
    private Boolean state;
    private int nbCharAlive;
    private int round;
    private PlayerThread soundThread;

    public Dungeon(final IConfiguration configuration) {
        this.configuration = configuration;
        this.cases = new Case[this.configuration.getWidth()][this.configuration.getHeight()];
        this.random = new Random();
        this.movable = new ArrayList<>();
        this.state = true;

        Case myCase;

        for (int y = 0; y < this.configuration.getHeight(); y++) {
            for (int x = 0; x < this.configuration.getWidth(); x++) {
                if ((x == 0) || (x == (this.configuration.getWidth() - 1)) || (y == 0)
                        || (y == (this.configuration.getHeight() - 1))) {
                    myCase = new Border();
                } else {
                    myCase = this.makeObstacle();
                }
                this.setXY(x, y, myCase);
            }

        }
        this.dungeon = new Plateau("JSim Donjon", this.configuration.getWidth(), this.configuration.getHeight(), this.cases, this);
        this.initImages();
        this.putMonster();
        this.putCharacters();
        this.nbCharAlive = this.configuration.getNbCharacters();
        this.round = 0;
        this.dungeon.setStats();
        try {
            this.soundThread = new PlayerThread(new RunnableSound("Textures/" + this.configuration.getTexturePack() + "/song.mp3"));
        } catch (Exception ex) {
            System.out.println("Echec de la lecture du fichier son !");
            Logger.getLogger(VisualiseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void show() {
        this.dungeon.rafraichir();
    }

    public MobilesElements getMobileElement(final int x, final int y) {
        for (final MobilesElements c : this.movable) {
            if (c != null) {
                if ((c.getX() == x) && (c.getY() == y)) {
                    return c;
                }
            }
        }
        return null;
    }

    private void placeRandomMobileElement(final MobilesElements c) {
        int x = this.random.nextInt(this.configuration.getWidth());
        int y = this.random.nextInt(this.configuration.getHeight());
        while ((!this.getXY(x, y).isEmpty()) && (this.getMobileElement(x, y) == null)) {
            x = this.random.nextInt(this.configuration.getWidth());
            y = this.random.nextInt(this.configuration.getHeight());
        }
        c.setX(x);
        c.setY(y);
    }

    private void putMonster() {
        final Monster monster = new Monster(this);
        this.movable.add(monster);
        this.placeRandomMobileElement(monster);
        this.dungeon.placerPiece(monster);
    }

    private void putCharacter() {
        MobilesElements c;
        switch (this.random.nextInt(nbTypesCharacters)) {
            case 0:
                c = new Scoot(this, this.movable.size());
                break;
            default:
                c = new Weak(this, this.movable.size());
                break;
        }
        putCharacter(c);
    }

    public void putCharacter(MobilesElements c) {
        this.movable.add(c);
        this.placeRandomMobileElement(c);
        this.dungeon.placerPiece(c);
    }

    private void putCharacters() {
        for (int i = 0; i < this.configuration.getNbCharacters(); i++) {
            putCharacter();
        }
    }

    private Case makeObstacle() {
        if (this.random.nextBoolean()) {
            if (this.random.nextInt(this.configuration.getDensityRock()) == 0) {
                return new Rock();
            } else {
                return new Empty();
            }
        } else {
            if (this.random.nextInt(this.configuration.getDensityWall()) == 0) {
                return new Wall();
            } else {
                return new Empty();
            }
        }
    }

    public Case getXY(final int x, final int y) {
        if (x >= 0 && y >= 0 && x < this.configuration.getWidth() && y < this.configuration.getHeight()) {
            return this.cases[x][y];
        } else {
            return new Border();
        }
    }

    public void play() {
        turnOffShine();
        for (final MobilesElements c : this.movable) {
            c.move();
        }
        turnOnShine();
        this.round++;
    }

    public void setXY(int x, int y, final Case c) {
        this.cases[x][y] = c;
    }

    private void initImages() {
        try {
            if (this.configuration.getTexturePack().equals("Default")) {
                this.dungeon.addImageToHashmap(1, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Empty.jpg")));
                this.dungeon.addImageToHashmap(2, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Wall.jpg")));
                this.dungeon.addImageToHashmap(3, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Rock.jpg")));
                this.dungeon.addImageToHashmap(5, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Scoot.jpg")));
                this.dungeon.addImageToHashmap(6, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Weak.jpg")));
                this.dungeon.addImageToHashmap(8, ImageIO.read(Dungeon.class.getResource("/Textures/Default/DeadMonster.jpg")));
                this.dungeon.addImageToHashmap(9, ImageIO.read(Dungeon.class.getResource("/Textures/Default/DeadCharacters.jpg")));
                this.dungeon.addImageToHashmap(0, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Border.jpg")));
                this.dungeon.addImageToHashmap(11, ImageIO.read(Dungeon.class.getResource("/Textures/Default/Shine.jpg")));
                this.dungeon.addImageToHashmap(12, ImageIO.read(Dungeon.class.getResource("/Textures/Default/CharEmpty.png")));
                this.dungeon.addImageToHashmap(20, ImageIO.read(Dungeon.class.getResource("/Textures/Default/MonsterNorth.jpg")));
                this.dungeon.addImageToHashmap(21, ImageIO.read(Dungeon.class.getResource("/Textures/Default/MonsterEast.jpg")));
                this.dungeon.addImageToHashmap(22, ImageIO.read(Dungeon.class.getResource("/Textures/Default/MonsterSouth.jpg")));
                this.dungeon.addImageToHashmap(23, ImageIO.read(Dungeon.class.getResource("/Textures/Default/MonsterWest.jpg")));
            } else {
                this.dungeon.addImageToHashmap(1, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Empty.jpg")));
                this.dungeon.addImageToHashmap(2, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Wall.jpg")));
                this.dungeon.addImageToHashmap(3, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Rock.jpg")));
                this.dungeon.addImageToHashmap(5, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Scoot.jpg")));
                this.dungeon.addImageToHashmap(6, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Weak.jpg")));
                this.dungeon.addImageToHashmap(8, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/DeadMonster.jpg")));
                this.dungeon.addImageToHashmap(9, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/DeadCharacters.jpg")));
                this.dungeon.addImageToHashmap(0, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Border.jpg")));
                this.dungeon.addImageToHashmap(11, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/Shine.jpg")));
                this.dungeon.addImageToHashmap(12, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/CharEmpty.png")));
                this.dungeon.addImageToHashmap(20, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/MonsterNorth.jpg")));
                this.dungeon.addImageToHashmap(21, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/MonsterEast.jpg")));
                this.dungeon.addImageToHashmap(22, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/MonsterSouth.jpg")));
                this.dungeon.addImageToHashmap(23, ImageIO.read(new File("Textures/" + this.configuration.getTexturePack() + "/MonsterWest.jpg")));
            }
        } catch (IOException ex) {
            Logger.getLogger(VisualiseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getxMonster() {
        return xMonster;
    }

    public void setMonsterPosition(int xMonster, int yMonster) {
        this.xMonster = xMonster;
        this.yMonster = yMonster;
    }

    public int getyMonster() {
        return yMonster;
    }

    public int getScootShow() {
        return scootShow;
    }

    public void setScootShow(int scootID) {
        this.scootShow = scootID;
    }

    public void setStepByStep(boolean status) {
        this.stepStatus = status;
    }

    public int getMovableSize() {
        return movable.size();
    }

    public void turnOnShine() {
        for (final MobilesElements c : this.movable) {
            if (c instanceof Characters && c.isAlive()) {
                if (this.getXY(c.getX(), c.getY() + 2).isEmpty() && !(this.getXY(c.getX(), c.getY() + 2) instanceof CharDead)) {
                    this.setXY(c.getX(), c.getY() + 2, new Shine());
                }
                if (this.getXY(c.getX(), c.getY() + 1).isEmpty() && !(this.getXY(c.getX(), c.getY() + 1) instanceof CharDead)) {
                    this.setXY(c.getX(), c.getY() + 1, new Shine());
                }
                if (this.getXY(c.getX() - 1, c.getY()).isEmpty() && !(this.getXY(c.getX() - 1, c.getY()) instanceof CharDead)) {
                    this.setXY(c.getX() - 1, c.getY(), new Shine());
                }
                if (this.getXY(c.getX() - 2, c.getY()).isEmpty() && !(this.getXY(c.getX() - 2, c.getY()) instanceof CharDead)) {
                    this.setXY(c.getX() - 2, c.getY(), new Shine());
                }
                if (this.getXY(c.getX(), c.getY() - 2).isEmpty() && !(this.getXY(c.getX(), c.getY() - 2) instanceof CharDead)) {
                    this.setXY(c.getX(), c.getY() - 2, new Shine());
                }
                if (this.getXY(c.getX(), c.getY() - 1).isEmpty() && !(this.getXY(c.getX(), c.getY() - 1) instanceof CharDead)) {
                    this.setXY(c.getX(), c.getY() - 1, new Shine());
                }
                if (this.getXY(c.getX() + 1, c.getY()).isEmpty() && !(this.getXY(c.getX() + 1, c.getY()) instanceof CharDead)) {
                    this.setXY(c.getX() + 1, c.getY(), new Shine());
                }
                if (this.getXY(c.getX() + 2, c.getY()).isEmpty() && !(this.getXY(c.getX() + 2, c.getY()) instanceof CharDead)) {
                    this.setXY(c.getX() + 2, c.getY(), new Shine());
                }
                if (this.getXY(c.getX() - 1, c.getY() - 1).isEmpty() && !(this.getXY(c.getX() - 1, c.getY() - 1) instanceof CharDead)) {
                    this.setXY(c.getX() - 1, c.getY() - 1, new Shine());
                }
                if (this.getXY(c.getX() + 1, c.getY() + 1).isEmpty() && !(this.getXY(c.getX() + 1, c.getY() + 1) instanceof CharDead)) {
                    this.setXY(c.getX() + 1, c.getY() + 1, new Shine());
                }
                if (this.getXY(c.getX() - 1, c.getY() + 1).isEmpty() && !(this.getXY(c.getX() - 1, c.getY() + 1) instanceof CharDead)) {
                    this.setXY(c.getX() - 1, c.getY() + 1, new Shine());
                }
                if (this.getXY(c.getX() + 1, c.getY() - 1).isEmpty() && !(this.getXY(c.getX() + 1, c.getY() - 1) instanceof CharDead)) {
                    this.setXY(c.getX() + 1, c.getY() - 1, new Shine());
                }
            }
            switch (c.getDirection()) {
                case NORTH:
                    if (this.getXY(c.getX(), c.getY() + 2).isEmpty() && this.getXY(c.getX(), c.getY() + 2) instanceof Shine) {
                        this.setXY(c.getX(), c.getY() + 2, new Empty());
                    }
                    if (this.getXY(c.getX(), c.getY() + 1).isEmpty() && this.getXY(c.getX(), c.getY() + 1) instanceof Shine) {
                        this.setXY(c.getX(), c.getY() + 1, new Empty());
                    }
                    break;
                case EAST:
                    if (this.getXY(c.getX() - 1, c.getY()).isEmpty() && this.getXY(c.getX(), c.getY() + 2) instanceof Shine) {
                        this.setXY(c.getX() - 1, c.getY(), new Empty());
                    }
                    if (this.getXY(c.getX() - 2, c.getY()).isEmpty() && this.getXY(c.getX(), c.getY() + 1) instanceof Shine) {
                        this.setXY(c.getX() - 2, c.getY(), new Empty());
                    }
                    break;
                case SOUTH:
                    if (this.getXY(c.getX(), c.getY() - 2).isEmpty() && this.getXY(c.getX(), c.getY() - 2) instanceof Shine) {
                        this.setXY(c.getX(), c.getY() - 2, new Empty());
                    }
                    if (this.getXY(c.getX(), c.getY() - 1).isEmpty() && this.getXY(c.getX(), c.getY() - 1) instanceof Shine) {
                        this.setXY(c.getX(), c.getY() - 1, new Empty());
                    }
                    break;
                case WEST:
                    if (this.getXY(c.getX() + 1, c.getY()).isEmpty() && this.getXY(c.getX() + 1, c.getY()) instanceof Shine) {
                        this.setXY(c.getX() + 1, c.getY(), new Empty());
                    }
                    if (this.getXY(c.getX() + 2, c.getY()).isEmpty() && this.getXY(c.getX() + 2, c.getY()) instanceof Shine) {
                        this.setXY(c.getX() + 2, c.getY(), new Empty());
                    }
                    break;
            }
        }
    }

    public void turnOffShine() {
        for (final MobilesElements c : this.movable) {
            if (c instanceof Characters) {
                for (int i = 0; i < this.configuration.getWidth(); i++) {
                    for (int j = 0; j < this.configuration.getHeight(); j++) {
                        if (this.getXY(i, j).isEmpty()) {
                            if (this.getXY(i, j) instanceof Shine) {
                                this.setXY(i, j, new Empty());
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean getState() {
        return this.state;
    }

    public void dieChar() {
        this.nbCharAlive--;
        if (nbCharAlive == 0) {
            setState(false);
        }
    }

    public void setState(boolean b) {
        this.state = b;
    }

    public Monster getMonster() {
        if (this.movable == null) {
            return new Monster(this);
        }
        return (Monster) this.movable.get(0);
    }

    public int getNbCharAlive() {
        return nbCharAlive;
    }

    public int getRound() {
        return round;
    }

    public void startSong() {
        this.soundThread.start();

    }
}
