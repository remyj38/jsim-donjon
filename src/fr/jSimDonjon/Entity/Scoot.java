package fr.jSimDonjon.Entity;

import fr.jSimDonjon.Case.Case;
import fr.jSimDonjon.Dungeon;
import static java.lang.Math.abs;

public class Scoot extends Characters {

    public Scoot(Dungeon dungeon, int id) {
        super(dungeon, "Scoot", 5, id);
    }

    @Override
    public void move() {
        if (this.isAlive) { // Si l'éclaireur est en vie, il bouge
            findMonster(); // Il recherche le monstre
        }
    }

    private void findMonster() { // Recherche du monstre

        if (!((this.dungeon.getxMonster() == -1) && (this.dungeon.getyMonster() == -1)) && (this.dungeon.getScootShow() == this.id)) { // Si le monstre est trouvé et que le personnage l'ayant trouvé est le personnage actuel
            this.dungeon.setMonsterPosition(-1, -1); // Le montre repasse au stade de non détecté
            this.dungeon.setScootShow(-1); // Le personnage l'ayant trouvé repasse sur aucun
        }
        Boolean monsterIsVisible = monsterVisible();
        if (this.pause <= 0) { // Si il n'est pas bloqué pour x tours, il bouge
            if (!monsterIsVisible && (this.dungeon.getxMonster() == -1) && (this.dungeon.getyMonster() == -1)) { // Si le monstre n'est pas trouvé dans les environs du personnage, on bouge normalement
                moveNormaly();
            } else { // Sinon, on le suit
                followMonster();
            }
        } else { // Sinon, on passe un tour sur la pause
            this.pause--;
        }
    }

    private void followMonster() { // Suivit du monstre
        int diffX;
        int diffY;
        diffX = this.getPositionHorizontale() - this.dungeon.getxMonster(); // Calcul de la distance Personnage Monstre sur l'axe X
        diffY = this.getPositionVerticale() - this.dungeon.getyMonster();// Calcul de la distance Personnage Monstre sur l'axe Y
        if (diffX == 0) { // Si la différence sur X est de 0, on va seulement se tourner au nord ou sud
            if (diffY < 0) { // Si la différence est négative, on oriente le personnage vers le sud
                if (this.direction != Direction.SOUTH) {
                    this.direction = Direction.SOUTH;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }

            } else { // Sinon, vers le nord
                if (this.direction != Direction.NORTH) {
                    this.direction = Direction.NORTH;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            }
        } else if (diffY == 0) { // Sinon, Si la différence sur y est de 0, on va seulement se tourner à l'est ou l'ouest

            if (diffX < 0) { // Si la différence est négative, on oriente le personnage vers l'est
                if (this.direction != Direction.EAST) {
                    this.direction = Direction.EAST;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            } else { // Sinon, vers l'ouest
                if (this.direction != Direction.WEST) {
                    this.direction = Direction.WEST;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            }
        } else if (abs(diffX) < abs(diffY)) { // Sinon, Si la valeur absolue de la différence sur X est inférieur à la différence sur Y, on oriente à l'est ou l'ouest
            if (diffX < 0) { // Si la différence est négative, on oriente le personnage vers l'est
                if (this.direction != Direction.EAST) {
                    this.direction = Direction.EAST;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            } else { // Sinon, vers l'ouest
                if (this.direction != Direction.WEST) {
                    this.direction = Direction.WEST;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            }
        } else { // Sinon, on oriente vers le nord ou sud
            if (diffY < 0) { // Si la différence est négative, on oriente le personnage vers le sud
                if (this.direction != Direction.SOUTH) {
                    this.direction = Direction.SOUTH;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            } else {
                if (this.direction != Direction.NORTH) {
                    this.direction = Direction.NORTH;
                    this.pause++; // On marque un temps de pause avant le prochain déplacement
                } else {
                    moveNormaly();
                }
            }
        }
    }

    private void moveNormaly() {
        Case frontCase = null;
        MobilesElements frontEntity = null;
        switch (this.direction) {
            case NORTH:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale(), this.getPositionVerticale() - 1);
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale(), this.getPositionVerticale() - 1);
                break;
            case SOUTH:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale(), this.getPositionVerticale() + 1);
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale(), this.getPositionVerticale() + 1);
                break;
            case WEST:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale() - 1, this.getPositionVerticale());
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale() - 1, this.getPositionVerticale());
                break;
            case EAST:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale() + 1, this.getPositionVerticale());
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale() + 1, this.getPositionVerticale());
                break;
        }
        if (frontCase.isEmpty() && frontEntity == null) {
            this.setX(this.newX());
            this.setY(this.newY());
        } else if (frontCase.isEmpty() && !frontEntity.isAlive) {
            this.setX(this.newX());
            this.setY(this.newY());
        } else {
            this.changeDirection();
        }
    }

}
