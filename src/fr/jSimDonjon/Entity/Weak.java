package fr.jSimDonjon.Entity;

import fr.jSimDonjon.Case.Case;
import fr.jSimDonjon.Case.Empty;
import fr.jSimDonjon.Case.Rock;
import fr.jSimDonjon.Dungeon;
import static java.lang.Math.abs;

public class Weak extends Characters {

    public Weak(Dungeon dungeon, int id) {
        super(dungeon, "Weak", 6, id);
    }

    @Override
    public void move() {
        if (this.isAlive) { // Si le perso est en vie
            if (this.pause <= 0) { // Si il n'est pas bloqué pour x tours, il bouge
                if (!((this.dungeon.getxMonster() == -1) && (this.dungeon.getyMonster() == -1)) && (this.dungeon.getScootShow() == this.id)) { // Si le monstre est trouvé et que le personnage l'ayant trouvé est le personnage actuel
                    this.dungeon.setMonsterPosition(-1, -1); // Le montre repasse au stade de non détecté
                    this.dungeon.setScootShow(-1); // Le personnage l'ayant trouvé repasse sur aucun
                }

                switch (this.direction) { // Suivant sa direction
                    case NORTH:
                        if (this.dungeon.getXY(this.getX(), this.getY() - 1).isRock()) { // Si la case devant lui est une pierre
                            moveRock(); // On bouge la pierre
                        } else {
                            leakMonster(); // Sinon, on fuit le monstre
                        }
                        break;
                    case SOUTH:
                        if (this.dungeon.getXY(this.getX(), this.getY() + 1).isRock()) { // Si la case devant lui est une pierre
                            moveRock(); // Si la case devant lui est une pierre
                        } else {
                            leakMonster(); // Sinon, on fuit le monstre
                        }
                        break;
                    case WEST:
                        if (this.dungeon.getXY(this.getX() - 1, this.getY()).isRock()) { // Si la case devant lui est une pierre
                            moveRock(); // Si la case devant lui est une pierre
                        } else {
                            leakMonster(); // Sinon, on fuit le monstre
                        }
                        break;
                    case EAST:
                        if (this.dungeon.getXY(this.getX() + 1, this.getY()).isRock()) { // Si la case devant lui est une pierre
                            moveRock(); // Si la case devant lui est une pierre
                        } else {
                            leakMonster(); // Sinon, on fuit le monstre
                        }
                        break;

                }
            } else {
                this.pause--;
            }
        }
    }

    public void moveRock() { // on bouge la pierre
        switch (this.direction) { // Suivant la direction
            case NORTH:
                if (this.dungeon.getXY(this.getX(), this.getY() - 2).isEmpty() && this.dungeon.getMobileElement(this.getX(), this.getY() - 2) == null) { // Si la case où on veut bouger la pierre est vide , on déplace la pierre et le personnage
                    this.dungeon.setXY(this.getX(), this.getY() - 2, new Rock());
                    this.dungeon.setXY(this.getX(), this.getY() - 1, new Empty());
                    moveNormaly();
                } else { // Sinon, on tourne
                    changeDirection();
                }
                break;
            case SOUTH:
                if (this.dungeon.getXY(this.getX(), this.getY() + 2).isEmpty() && this.dungeon.getMobileElement(this.getX(), this.getY() + 2) == null) {// Si la case où on veut bouger la pierre est vide , on déplace la pierre et le personnage
                    this.dungeon.setXY(this.getX(), this.getY() + 2, new Rock());
                    this.dungeon.setXY(this.getX(), this.getY() + 1, new Empty());
                    moveNormaly();
                } else { // Sinon, on tourne
                    changeDirection();
                }
                break;
            case WEST:
                if (this.dungeon.getXY(this.getX() - 2, this.getY()).isEmpty() && this.dungeon.getMobileElement(this.getX() - 2, this.getY()) == null) {// Si la case où on veut bouger la pierre est vide , on déplace la pierre et le personnage
                    this.dungeon.setXY(this.getX() - 2, this.getY(), new Rock());
                    this.dungeon.setXY(this.getX() - 1, this.getY(), new Empty());
                    moveNormaly();
                } else { // Sinon, on tourne
                    changeDirection();
                }
                break;
            case EAST:
                if (this.dungeon.getXY(this.getX() + 2, this.getY()).isEmpty() && this.dungeon.getMobileElement(this.getX() + 2, this.getY()) == null) {// Si la case où on veut bouger la pierre est vide , on déplace la pierre et le personnage
                    this.dungeon.setXY(this.getX() + 2, this.getY(), new Rock());
                    this.dungeon.setXY(this.getX() + 1, this.getY(), new Empty());
                    moveNormaly();
                } else { // Sinon, on tourne
                    changeDirection();
                }
                break;
        }
    }

    private void leakMonster() { // Suivit du monstre
        Boolean monsterIsVisible = monsterVisible();
        if (!monsterIsVisible && (this.dungeon.getxMonster() == -1) && (this.dungeon.getyMonster() == -1)) { // Si le monstre n'est pas trouvé dans les environs du personnage, on bouge normalement
            moveNormaly();
        } else {
            int diffX;
            int diffY;
            diffX = this.getPositionHorizontale() - this.dungeon.getxMonster(); // Calcul de la distance Personnage Monstre sur l'axe X
            diffY = this.getPositionVerticale() - this.dungeon.getyMonster();// Calcul de la distance Personnage Monstre sur l'axe Y
            if (diffX == 0) { // Si la différence sur X est de 0, on va seulement se tourner au nord ou sud
                if (diffY > 0) { // Si la différence est positive, on oriente le personnage vers le sud
                    if (this.direction != Direction.SOUTH) {
                        this.direction = Direction.SOUTH;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }

                } else { // Sinon, vers le nord
                    if (this.direction != Direction.NORTH) {
                        this.direction = Direction.NORTH;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                }
            } else if (diffY == 0) { // Sinon, Si la différence sur y est de 0, on va seulement se tourner à l'est ou l'ouest

                if (diffX > 0) { // Si la différence est positive, on oriente le personnage vers l'est
                    if (this.direction != Direction.EAST) {
                        this.direction = Direction.EAST;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                } else { // Sinon, vers l'ouest
                    if (this.direction != Direction.WEST) {
                        this.direction = Direction.WEST;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                }
            } else if (abs(diffX) < abs(diffY)) { // Sinon, Si la valeur absolue de la différence sur X est suppérieur à la différence sur Y, on oriente à l'est ou l'ouest
                if (diffX > 0) { // Si la différence est positive, on oriente le personnage vers l'est
                    if (this.direction != Direction.EAST) {
                        this.direction = Direction.EAST;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                } else { // Sinon, vers l'ouest
                    if (this.direction != Direction.WEST) {
                        this.direction = Direction.WEST;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                }
            } else { // Sinon, on oriente vers le nord ou sud
                if (diffY > 0) { // Si la différence est négative, on oriente le personnage vers le sud
                    if (this.direction != Direction.SOUTH) {
                        this.direction = Direction.SOUTH;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                } else {
                    if (this.direction != Direction.NORTH) {
                        this.direction = Direction.NORTH;
                        this.pause++; // On marque un temps de pause avant le prochain déplacement
                    } else {
                        moveNormaly();
                    }
                }
            }
        }
    }

    private void moveNormaly() {
        Case frontCase = null;
        MobilesElements frontEntity = null;
        switch (this.direction) {
            case NORTH:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale(), this.getPositionVerticale() - 1);
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale(), this.getPositionVerticale() - 1);
                break;
            case SOUTH:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale(), this.getPositionVerticale() + 1);
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale(), this.getPositionVerticale() + 1);
                break;
            case WEST:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale() - 1, this.getPositionVerticale());
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale() - 1, this.getPositionVerticale());
                break;
            case EAST:
                frontCase = this.dungeon.getXY(this.getPositionHorizontale() + 1, this.getPositionVerticale());
                frontEntity = this.dungeon.getMobileElement(this.getPositionHorizontale() + 1, this.getPositionVerticale());
                break;
        }
        if (frontCase != null) {
            if (frontCase.isEmpty() && frontEntity == null) {
                this.setX(this.newX());
                this.setY(this.newY());
            } else if (frontCase.isEmpty() && !frontEntity.isAlive) {
                this.setX(this.newX());
                this.setY(this.newY());
            } else {
                this.changeDirection();
            }
        } else {
            this.changeDirection();
        }
    }

}
