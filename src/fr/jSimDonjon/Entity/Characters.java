package fr.jSimDonjon.Entity;

import fr.jSimDonjon.Dungeon;

public class Characters extends MobilesElements {

    protected final int id;

    public Characters(Dungeon dungeon, String typeChar, int image, int id) {
        super(dungeon, typeChar, image);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void die() {
        super.die(12);
    }

    @Override
    public void changeDirection() {
        if (this.direction.ordinal() != 0) {
            this.direction = Direction.values()[this.direction.ordinal() - 1];
        } else {
            this.direction = Direction.values()[3];
        }
        this.pause++;
    }

    protected Boolean monsterVisible() {
        int x = this.getPositionHorizontale();
        int y = this.getPositionVerticale();
        if (!testMonster(x, y - 2)) {
            return false;
        }
        if (!testMonster(x, y - 1)) {
            return false;
        }
        if (!testMonster(x + 1, y - 1)) {
            return false;
        }
        if (!testMonster(x + 1, y)) {
            return false;
        }
        if (!testMonster(x + 2, y)) {
            return false;
        }
        if (!testMonster(x + 1, y - 1)) {
            return false;
        }
        if (!testMonster(x, y + 1)) {
            return false;
        }
        if (!testMonster(x, y + 2)) {
            return false;
        }
        if (!testMonster(x - 1, y + 1)) {
            return false;
        }
        if (!testMonster(x + 1, y)) {
            return false;
        }
        if (!testMonster(x - 2, y)) {
            return false;
        }
        if (!testMonster(x - 1, y - 1)) {
            return false;
        }

        switch (this.direction) {
            case NORTH:
                if ((this.dungeon.getxMonster() == x && this.dungeon.getyMonster() == y + 1) || (this.dungeon.getxMonster() == x && this.dungeon.getyMonster() == y + 2)) {
                    return false;
                } else {
                    return true;
                }
            case EAST:
                if ((this.dungeon.getxMonster() == x - 1 && this.dungeon.getyMonster() == y) || (this.dungeon.getxMonster() == x - 2 && this.dungeon.getyMonster() == y)) {
                    return false;
                } else {
                    return true;
                }
            case SOUTH:
                if ((this.dungeon.getxMonster() == x && this.dungeon.getyMonster() == y - 1) || (this.dungeon.getxMonster() == x && this.dungeon.getyMonster() == y - 2)) {
                    return false;
                } else {
                    return true;
                }
            case WEST:
                if ((this.dungeon.getxMonster() == x + 1 && this.dungeon.getyMonster() == y) || (this.dungeon.getxMonster() == x + 2 && this.dungeon.getyMonster() == y)) {
                    return false;
                } else {
                    return true;
                }

        }

        return false;
    }

    private Boolean testMonster(int x, int y) {
        MobilesElements e = this.dungeon.getMobileElement(x, y);
        if (e != null) {
            if (e instanceof Monster) {
                this.dungeon.setMonsterPosition(x, y);
                this.dungeon.setScootShow(this.id);
                return true;
            }
        }
        return false;
    }

}
