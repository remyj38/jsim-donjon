package fr.jSimDonjon.Entity;

import fr.jSimDonjon.Dungeon;
import java.util.Random;

public abstract class MobilesElements implements fr.affichagePlateau.PlateauPiece {

    private int x;
    private int y;
    private int image;
    protected boolean isAlive;
    protected Direction direction;
    protected String typeChar;
    protected Dungeon dungeon;
    protected int pause;

    public MobilesElements(final Dungeon dungeon, final String typeChar, final int image) {
        this.dungeon = dungeon;
        this.typeChar = typeChar;
        this.image = image;
        this.direction = Direction.values()[new Random().nextInt(4)];
        this.pause = 0;
        this.isAlive = true;
    }

    public int newY() {
        switch (this.direction) {
            case NORTH: {
                if (this.y > 0) {
                    return this.y - 1;
                }
            }
            break;
            case SOUTH: {
                if (this.y < (this.dungeon.configuration.getHeight() - 1)) {
                    return this.y + 1;
                }
            }
            break;
            default:
                return this.y;
        }
        return y;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public int newX() {
        switch (this.direction) {
            case EAST: {
                if (this.x < (this.dungeon.configuration.getWidth() - 1)) {
                    return this.x + 1;
                }
            }
            break;
            case WEST: {
                if (this.x > 0) {
                    return this.x - 1;
                }
            }
            break;
            default:
                return this.x;
        }
        return x;
    }

    @Override
    public int getPositionHorizontale() {
        return this.getX();
    }

    @Override
    public int getPositionVerticale() {
        return this.getY();
    }

    public String getTypeChar() {
        return typeChar;
    }

    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    @Override
    public Integer getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setY(final int y) {
        this.y = y;
    }

    public abstract void changeDirection();

    public void move() {
        
        final int x = this.newX();
        final int y = this.newY();
        
        if (this.dungeon.getXY(x, y).isEmpty())
        {
            this.x = x;
            this.y = y;
        }
        this.changeDirection();
    }

    public void die(int image) {
        this.isAlive = false;
        this.setImage(image);
    }

    public Direction getDirection() {
        return direction;
    }
    
}
