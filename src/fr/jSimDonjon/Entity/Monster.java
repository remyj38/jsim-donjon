package fr.jSimDonjon.Entity;

import fr.jSimDonjon.Case.CharDead;
import fr.jSimDonjon.Dungeon;

public class Monster extends MobilesElements {

    private int nbDirectionChange;
    private int healthPoints;

    public Monster(Dungeon myDungeon) {
        super(myDungeon, "Monster", 20);
        this.healthPoints = 100;
        this.nbDirectionChange = 0;
    }

    @Override
    public void move() {
        if (this.isAlive == true) {
            if (this.pause == 0) {
                if (!test()) {
                    super.move();
                    healthPoints--;
                }
                if (healthPoints == 0) {
                    die(8);
                    this.dungeon.setState(false);
                }
            } else if (this.pause != 0) {
                this.pause--;
            }
        }
    }

    @Override
    public void changeDirection() {
        final int x = this.newX();
        final int y = this.newY();

        if (!this.dungeon.getXY(x, y).isEmpty()) {
            this.pause++;
            if (nbDirectionChange < 3) {
                if ((this.direction.ordinal() + 1) >= 4) {
                    this.direction = Direction.values()[this.direction.ordinal() - 3];
                } else {
                    this.direction = Direction.values()[this.direction.ordinal() + 1];
                }
                nbDirectionChange++;
            } else {
                this.pause++;
                if ((this.direction.ordinal() + 2) >= 4) {
                    this.direction = Direction.values()[this.direction.ordinal() - 2];
                } else {
                    this.direction = Direction.values()[this.direction.ordinal() + 2];
                }
                nbDirectionChange = 0;
            }
        }
        this.setImage(20 + this.direction.ordinal());
    }

    public boolean test() {
        testCase(this.dungeon.getMobileElement(this.getX(), this.getY()));
        switch (this.direction) {
            case NORTH:
                if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() - 1))) { // case Nord
                    if (!testCase(this.dungeon.getMobileElement(this.getX() + 1, this.getY()))) {// case Est
                        if (!testCase(this.dungeon.getMobileElement(this.getX() - 1, this.getY()))) {// case Ouest
                            return false;
                        } else {
                            this.direction = Direction.WEST;
                        }
                    } else {
                        this.direction = Direction.EAST;
                    }
                } else {
                    this.direction = Direction.NORTH;
                }
                break;

            case EAST:
                if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() + 1)))// case du Sud
                {
                    if (!testCase(this.dungeon.getMobileElement(this.getX() + 1, this.getY())))// case de Est
                    {
                        if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() - 1))) // case du Nord
                        {
                            return false;
                        } else {
                            this.direction = Direction.NORTH;
                        }
                    } else {
                        this.direction = Direction.EAST;
                    }
                } else {
                    this.direction = Direction.SOUTH;
                }
                break;
            case SOUTH:
                if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() + 1)))// case du Sud
                {
                    if (!testCase(this.dungeon.getMobileElement(this.getX() + 1, this.getY())))// case de Est
                    {
                        if (!testCase(this.dungeon.getMobileElement(this.getX() - 1, this.getY())))// case de Ouest
                        {
                            return false;
                        } else {
                            this.direction = Direction.WEST;
                        }
                    } else {
                        this.direction = Direction.EAST;
                    }
                } else {
                    this.direction = Direction.SOUTH;
                }
                break;
            case WEST:
                if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() + 1)))// case du Sud
                {
                    if (!testCase(this.dungeon.getMobileElement(this.getX() - 1, this.getY())))// case de Ouest
                    {
                        if (!testCase(this.dungeon.getMobileElement(this.getX(), this.getY() - 1))) // case du Nord
                        {
                            return false;
                        }
                    }
                }
                this.direction = Direction.NORTH;
                break;
        }
        return true;
    }

    public boolean testCase(final MobilesElements e) {
        if (e != null) {
            if (e instanceof Characters) {
                final Characters c = (Characters) e;
                if (c.isAlive == true) {
                    this.setX(c.getX());
                    this.setY(c.getY());
                    c.die();
                    this.dungeon.setXY(c.getX(), c.getY(), new CharDead());
                    healthPoints = healthPoints + 20;
                    this.pause = pause + 3;
                    this.dungeon.dieChar();
                    return true;
                }
            }
        }
        return false;
    }

    public String getHealthPoints() {
        return  String.valueOf(healthPoints);
    }
    
}
