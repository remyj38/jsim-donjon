package fr.jSimDonjon;

import fr.jSimDonjon.Configuration.ConfigurationWindow;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JSimDonjon {

    public static void main(String[] args) {
        final ConfigurationDungeon myConfig = new ConfigurationDungeon();
        final ConfigurationWindow configurationWindow = new ConfigurationWindow(myConfig);
        if (configurationWindow.getAnswer().equals("Ok")) {
            final Dungeon myDungeon = new Dungeon(myConfig);
            if (myConfig.getSound()) {
                myDungeon.startSong();
            }
            while (myDungeon.getState()) {
                if (!myDungeon.stepStatus) {
                    myDungeon.play();
                    myDungeon.show();
                }
                if (myConfig.getStepByStep()) {
                    myDungeon.stepStatus = true;
                }

                try {
                    Thread.sleep(myConfig.getRefreshTime());
                } catch (InterruptedException ex) {
                    Logger.getLogger(JSimDonjon.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

}
