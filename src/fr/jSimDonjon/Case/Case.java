package fr.jSimDonjon.Case;

public class Case implements fr.affichagePlateau.PlateauCase {

    protected Integer image = null;
    protected Boolean empty;
    protected Boolean rock;

    public Case(final Integer image, final Boolean empty, final Boolean rock) {
        this.empty = empty;
        this.image = image;
        this.rock = rock;
    }
    public Case(final Integer image, final Boolean empty) {
        this.empty = empty;
        this.image = image;
        this.rock = false;
    }

    public Boolean isEmpty() {
        return this.empty;
    }

    @Override
    public Integer getImage() {
        return this.image;
    }
    
    public Boolean isRock() {
        return this.rock;
    }
}
