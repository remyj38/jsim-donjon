package fr.jSimDonjon.Case;

public class Obstacle extends Case {
    
	public Obstacle(final int image)
	{
		super(image, false, false);
	}
        public Obstacle(final int image, Boolean rock)
	{
		super(image, false, rock);
	}
}
