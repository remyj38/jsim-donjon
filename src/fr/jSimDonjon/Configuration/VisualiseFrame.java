package fr.jSimDonjon.Configuration;

import fr.affichagePlateau.Plateau;
import fr.jSimDonjon.Case.Border;
import fr.jSimDonjon.Case.Case;
import fr.jSimDonjon.Case.Empty;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class VisualiseFrame {

    private final Plateau tray;
    private final IConfiguration config;

    public VisualiseFrame(IConfiguration config) {
        this.config = config;
        Case[][] trayCases = new Case[config.getWidth()][config.getHeight()];
        for (int y = 0; y < config.getHeight(); y++) {
            for (int x = 0; x < config.getWidth(); x++) {
                if (y == 0 || y == config.getHeight() - 1 || x == 0 || x == config.getWidth() - 1) {
                    trayCases[x][y] = new Border();
                } else {
                    trayCases[x][y] = new Empty();
                }
            }
        }
        tray = new Plateau("Visualisation", config.getWidth(), config.getHeight(), trayCases, null);
        this.initImages();

    }

    private void initImages() {
        try {
            if (config.getTexturePack().equals("Default")) {
                this.tray.addImageToHashmap(1, ImageIO.read(VisualiseFrame.class.getResource("/Textures/Default/Empty.jpg")));
                this.tray.addImageToHashmap(2, ImageIO.read(VisualiseFrame.class.getResource("/Textures/Default/Wall.jpg")));
                this.tray.addImageToHashmap(3, ImageIO.read(VisualiseFrame.class.getResource("/Textures/Default/Rock.jpg")));
                this.tray.addImageToHashmap(0, ImageIO.read(VisualiseFrame.class.getResource("/Textures/Default/Border.jpg")));
            } else {
                this.tray.addImageToHashmap(1, ImageIO.read(new File("Textures/" + config.getTexturePack() + "/Empty.jpg")));
                this.tray.addImageToHashmap(2, ImageIO.read(new File("Textures/" + config.getTexturePack() + "/Wall.jpg")));
                this.tray.addImageToHashmap(3, ImageIO.read(new File("Textures/" + config.getTexturePack() + "/Rock.jpg")));
                this.tray.addImageToHashmap(0, ImageIO.read(new File("Textures/" + config.getTexturePack() + "/Border.jpg")));
            }
        } catch (IOException ex) {
            Logger.getLogger(VisualiseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteFrame() {
        this.tray.deleteFrame();
    }
}
