package fr.jSimDonjon.Configuration;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;

public class ConfigurationWindow extends JDialog {

    private static final long serialVersionUID = 1823220579238313073L;
    private final JPanel contentPanel = new JPanel();
    private JButton okButton;
    private JButton cancelButton;
    private String answer = "Cancel";
    private JSpinner width;
    private JSpinner height;
    private JSpinner densityRock;
    private JSpinner densityWall;
    private JSpinner nbCharacters;
    private JSpinner refreshTime;
    private JCheckBox stepByStep;
    private VisualiseFrame visualiseFrame;
    private JComboBox texturePack;
    private JCheckBox sound;

    public ConfigurationWindow(final IConfiguration configurations) {
        this.setModalityType(ModalityType.APPLICATION_MODAL);
        this.setResizable(false);
        this.setTitle("Configuration");
        this.setBounds(100, 100, 290, 300);
        this.getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.getContentPane().add(this.contentPanel, BorderLayout.CENTER);
        final GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{140, 80};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPanel.setLayout(gbl_contentPanel);
        {
            final JLabel labelWidth = new JLabel("Largeur :");
            final GridBagConstraints gbc_labelWidth = new GridBagConstraints();
            gbc_labelWidth.anchor = GridBagConstraints.EAST;
            gbc_labelWidth.insets = new Insets(0, 0, 5, 5);
            gbc_labelWidth.gridx = 0;
            gbc_labelWidth.gridy = 0;
            this.contentPanel.add(labelWidth, gbc_labelWidth);
        }
        {
            this.width = new JSpinner();
            GridBagConstraints gbc_width = new GridBagConstraints();
            gbc_width.insets = new Insets(0, 0, 5, 0);
            gbc_width.gridx = 1;
            gbc_width.gridy = 0;
            gbc_width.ipadx = 100;
            gbc_width.ipady = 0;
            contentPanel.add(width, gbc_width);
            this.width.setValue(configurations.getWidth());
        }
        {
            final JLabel labelHeight = new JLabel("Hauteur :");
            final GridBagConstraints gbc_labelHeight = new GridBagConstraints();
            gbc_labelHeight.anchor = GridBagConstraints.EAST;
            gbc_labelHeight.insets = new Insets(0, 0, 5, 5);
            gbc_labelHeight.gridx = 0;
            gbc_labelHeight.gridy = 1;
            this.contentPanel.add(labelHeight, gbc_labelHeight);
        }
        {
            this.height = new JSpinner();
            GridBagConstraints gbc_height = new GridBagConstraints();
            gbc_height.insets = new Insets(0, 0, 5, 0);
            gbc_height.gridx = 1;
            gbc_height.gridy = 1;
            gbc_height.ipadx = 100;
            gbc_height.ipady = 0;
            contentPanel.add(height, gbc_height);
            this.height.setValue(configurations.getHeight());
        }
        {
            final JLabel labelRock = new JLabel("Densit\u00E9 de pierres :");
            final GridBagConstraints gbc_labelRock = new GridBagConstraints();
            gbc_labelRock.anchor = GridBagConstraints.EAST;
            gbc_labelRock.insets = new Insets(0, 0, 5, 5);
            gbc_labelRock.gridx = 0;
            gbc_labelRock.gridy = 2;
            this.contentPanel.add(labelRock, gbc_labelRock);
        }
        {
            this.densityRock = new JSpinner();
            GridBagConstraints gbc_densityRock = new GridBagConstraints();
            gbc_densityRock.insets = new Insets(0, 0, 5, 0);
            gbc_densityRock.gridx = 1;
            gbc_densityRock.gridy = 2;
            gbc_densityRock.ipadx = 100;
            gbc_densityRock.ipady = 0;
            contentPanel.add(densityRock, gbc_densityRock);
            this.densityRock.setValue(configurations.getDensityRock());
        }
        {
            JLabel labelDensityWall = new JLabel("Densit\u00E9 de murs :");
            GridBagConstraints gbc_labelDensityWall = new GridBagConstraints();
            gbc_labelDensityWall.anchor = GridBagConstraints.EAST;
            gbc_labelDensityWall.insets = new Insets(0, 0, 5, 5);
            gbc_labelDensityWall.gridx = 0;
            gbc_labelDensityWall.gridy = 3;
            contentPanel.add(labelDensityWall, gbc_labelDensityWall);
        }
        {
            this.densityWall = new JSpinner();
            GridBagConstraints gbc_densityWall = new GridBagConstraints();
            gbc_densityWall.insets = new Insets(0, 0, 5, 0);
            gbc_densityWall.gridx = 1;
            gbc_densityWall.gridy = 3;
            gbc_densityWall.ipadx = 100;
            gbc_densityWall.ipady = 0;
            contentPanel.add(densityWall, gbc_densityWall);
            this.densityWall.setValue(configurations.getDensityWall());
        }
        {
            final JLabel lblNombreDenfants = new JLabel("Nombre de personnages :");
            final GridBagConstraints gbc_lblNombreDenfants = new GridBagConstraints();
            gbc_lblNombreDenfants.anchor = GridBagConstraints.EAST;
            gbc_lblNombreDenfants.insets = new Insets(0, 0, 5, 5);
            gbc_lblNombreDenfants.gridx = 0;
            gbc_lblNombreDenfants.gridy = 4;
            this.contentPanel.add(lblNombreDenfants, gbc_lblNombreDenfants);
        }
        {
            this.nbCharacters = new JSpinner();
            GridBagConstraints gbc_nbCharacters = new GridBagConstraints();
            gbc_nbCharacters.insets = new Insets(0, 0, 5, 0);
            gbc_nbCharacters.gridx = 1;
            gbc_nbCharacters.gridy = 4;
            gbc_nbCharacters.ipadx = 100;
            gbc_nbCharacters.ipady = 0;
            contentPanel.add(nbCharacters, gbc_nbCharacters);
            this.nbCharacters.setValue(configurations.getNbCharacters());
        }
        {
            JLabel labelRefreshTime = new JLabel("Temps de rafraichissement :");
            GridBagConstraints gbc_labelRefreshTime = new GridBagConstraints();
            gbc_labelRefreshTime.anchor = GridBagConstraints.EAST;
            gbc_labelRefreshTime.insets = new Insets(0, 0, 5, 5);
            gbc_labelRefreshTime.gridx = 0;
            gbc_labelRefreshTime.gridy = 5;
            contentPanel.add(labelRefreshTime, gbc_labelRefreshTime);
        }
        {
            this.refreshTime = new JSpinner();
            GridBagConstraints gbc_refreshTime = new GridBagConstraints();
            gbc_refreshTime.insets = new Insets(0, 0, 5, 0);
            gbc_refreshTime.gridx = 1;
            gbc_refreshTime.gridy = 5;
            gbc_refreshTime.ipadx = 100;
            gbc_refreshTime.ipady = 0;
            contentPanel.add(refreshTime, gbc_refreshTime);
            this.refreshTime.setValue(configurations.getRefreshTime());
        }
        {
            JLabel labelStepByStep = new JLabel("Mode pas \u00E0 pas :");
            GridBagConstraints gbc_labelStepByStep = new GridBagConstraints();
            gbc_labelStepByStep.anchor = GridBagConstraints.EAST;
            gbc_labelStepByStep.insets = new Insets(0, 0, 5, 5);
            gbc_labelStepByStep.gridx = 0;
            gbc_labelStepByStep.gridy = 6;
            contentPanel.add(labelStepByStep, gbc_labelStepByStep);
        }
        {
            this.stepByStep = new JCheckBox("");
            GridBagConstraints gbc_stepByStep = new GridBagConstraints();
            gbc_stepByStep.insets = new Insets(0, 0, 5, 0);
            gbc_stepByStep.gridx = 1;
            gbc_stepByStep.gridy = 6;
            contentPanel.add(stepByStep, gbc_stepByStep);
            this.stepByStep.setSelected(configurations.getStepByStep());
            {
                JLabel labelTexturepack = new JLabel("Textures :");
                GridBagConstraints gbc_labelTexturepack = new GridBagConstraints();
                gbc_labelTexturepack.insets = new Insets(0, 0, 5, 5);
                gbc_labelTexturepack.anchor = GridBagConstraints.EAST;
                gbc_labelTexturepack.gridx = 0;
                gbc_labelTexturepack.gridy = 8;
                contentPanel.add(labelTexturepack, gbc_labelTexturepack);
            }
            {
                this.texturePack = new JComboBox();
                this.texturePack.addItem("Default");
                getTexturesPacks(new File("Textures").list());
                this.texturePack.setSelectedItem("Default");
                GridBagConstraints gbc_texturePack = new GridBagConstraints();
                gbc_texturePack.insets = new Insets(0, 0, 5, 0);
                gbc_texturePack.fill = GridBagConstraints.HORIZONTAL;
                gbc_texturePack.gridx = 1;
                gbc_texturePack.gridy = 8;
                contentPanel.add(texturePack, gbc_texturePack);
                this.texturePack.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        sound.setSelected(configurations.getSound());
                        sound.setEnabled(testSong((String) texturePack.getSelectedItem()));

                    }
                });
            }
            {
                JLabel labelSound = new JLabel("Activer la musique :");
                GridBagConstraints gbc_labelSound = new GridBagConstraints();
                gbc_labelSound.anchor = GridBagConstraints.EAST;
                gbc_labelSound.insets = new Insets(0, 0, 0, 5);
                gbc_labelSound.gridx = 0;
                gbc_labelSound.gridy = 7;
                contentPanel.add(labelSound, gbc_labelSound);
            }
            {
                this.sound = new JCheckBox("");
                GridBagConstraints gbc_sound = new GridBagConstraints();
                gbc_sound.gridx = 1;
                gbc_sound.gridy = 7;
                contentPanel.add(sound, gbc_sound);
                this.sound.setSelected(configurations.getSound());
                this.sound.setEnabled(testSong(configurations.getTexturePack()));
            }
            this.stepByStep.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (ConfigurationWindow.this.stepByStep.isSelected() == true) {
                        ConfigurationWindow.this.refreshTime.setValue(0);
                        ConfigurationWindow.this.refreshTime.setEnabled(false);
                    } else {
                        ConfigurationWindow.this.refreshTime.setValue(configurations.getRefreshTime());
                        ConfigurationWindow.this.refreshTime.setEnabled(true);
                    }

                }
            });
        }
        {
            final JPanel buttonPane = new JPanel();
            FlowLayout fl_buttonPane = new FlowLayout(FlowLayout.CENTER);
            buttonPane.setLayout(fl_buttonPane);
            this.getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                this.okButton = new JButton("OK");
                this.okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent e) {
                        configurations.setHeight((Integer) ConfigurationWindow.this.height.getValue());
                        configurations.setWidth((Integer) ConfigurationWindow.this.width.getValue());
                        configurations.setDensityRock((Integer) ConfigurationWindow.this.densityRock.getValue());
                        configurations.setDensityWall((Integer) ConfigurationWindow.this.densityWall.getValue());
                        configurations.setNbCharacters((Integer) ConfigurationWindow.this.nbCharacters.getValue());
                        configurations.setRefreshTime((Integer) ConfigurationWindow.this.refreshTime.getValue());
                        configurations.setStepByStep(ConfigurationWindow.this.stepByStep.isSelected());
                        configurations.setTexturepack((String) ConfigurationWindow.this.texturePack.getSelectedItem());
                        configurations.setSound(ConfigurationWindow.this.sound.isSelected());
                        if (ConfigurationWindow.this.visualiseFrame != null) {
                            ConfigurationWindow.this.visualiseFrame.deleteFrame();
                        }
                        ConfigurationWindow.this.answer = "Ok";
                        ConfigurationWindow.this.setVisible(false);
                    }
                });
                {
                    JButton buttonVisualise = new JButton("Visualiser");
                    buttonPane.add(buttonVisualise);
                    buttonVisualise.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            configurations.setHeight((Integer) ConfigurationWindow.this.height.getValue());
                            configurations.setWidth((Integer) ConfigurationWindow.this.width.getValue());
                            configurations.setDensityRock((Integer) ConfigurationWindow.this.densityRock.getValue());
                            configurations.setDensityWall((Integer) ConfigurationWindow.this.densityWall.getValue());
                            configurations.setNbCharacters((Integer) ConfigurationWindow.this.nbCharacters.getValue());
                            configurations.setRefreshTime((Integer) ConfigurationWindow.this.refreshTime.getValue());
                            configurations.setStepByStep(ConfigurationWindow.this.stepByStep.isSelected());
                            configurations.setTexturepack((String) ConfigurationWindow.this.texturePack.getSelectedItem());
                            if (ConfigurationWindow.this.visualiseFrame != null) {
                                ConfigurationWindow.this.visualiseFrame.deleteFrame();
                            }
                            ConfigurationWindow.this.visualiseFrame = new VisualiseFrame(configurations);
                        }
                    });
                }
                this.okButton.setActionCommand("OK");
                buttonPane.add(this.okButton);
                this.getRootPane().setDefaultButton(this.okButton);
            }
            {
                this.cancelButton = new JButton("Cancel");
                this.cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent e) {
                        ConfigurationWindow.this.answer = "Cancel";
                        ConfigurationWindow.this.dispose();
                        if (ConfigurationWindow.this.visualiseFrame != null) {
                            ConfigurationWindow.this.visualiseFrame.deleteFrame();
                        }
                    }
                });
                this.cancelButton.setActionCommand("Cancel");
                buttonPane.add(this.cancelButton);
            }
        }
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public String getAnswer() {
        return this.answer;
    }

    private void getTexturesPacks(String[] contentDirectory) {
        int i = 0;
        for (String dir : contentDirectory) {
            if (new File("Textures/" + dir).isDirectory()) {
                this.texturePack.addItem(dir);
            }
        }
    }

    private Boolean testSong(String texturePack) {
        if (!texturePack.equals("Default")) {
            File[] directoryFiles = new File("Textures/" + texturePack).listFiles();
            for (File file : directoryFiles) {
                if (file.getName().equals("song.mp3") && file.isFile()) {
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }
}
