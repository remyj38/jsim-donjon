package fr.jSimDonjon.Configuration;

public interface IConfiguration {

    int getDensityRock();

    int getDensityWall();

    int getHeight();

    int getWidth();

    int getNbCharacters();

    int getRefreshTime();

    Boolean getStepByStep();

    String getTexturePack();

    Boolean getSound();

    void setDensityRock(int rock);

    void setDensityWall(int wall);

    void setHeight(int height);

    void setWidth(int width);

    void setNbCharacters(int nbCharacters);

    void setRefreshTime(int refreshTime);

    void setStepByStep(Boolean status);

    void setTexturepack(String texturePack);

    void setSound(Boolean sound);

}
