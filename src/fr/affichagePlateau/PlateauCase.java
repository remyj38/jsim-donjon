package fr.affichagePlateau;


public abstract interface PlateauCase
{
  public abstract Integer getImage();
}