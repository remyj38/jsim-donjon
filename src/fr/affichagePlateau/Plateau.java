package fr.affichagePlateau;

import fr.jSimDonjon.Dungeon;
import java.awt.Image;

public class Plateau {

    private final PlateauFenetre plateauFenetre;

    public Plateau(String title, int largeur, int hauteur, PlateauCase[][] plateauCases, Dungeon dungeon) {
        this.plateauFenetre = new PlateauFenetre(title, largeur, hauteur, plateauCases, dungeon);
    }

    public void placerPiece(PlateauPiece piece) {
        this.plateauFenetre.placerPiece(piece);
    }

    public void rafraichir() {
        this.plateauFenetre.rafraichir();
    }

    public void addImageToHashmap(Integer id, Image image) {
        this.plateauFenetre.addImageToHashmap(id, image);
    }

    public void deleteFrame() {
        this.plateauFenetre.dispose();
    }
    public void setStats(){
        this.plateauFenetre.setStats();
    }
}
