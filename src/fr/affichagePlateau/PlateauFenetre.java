package fr.affichagePlateau;

import fr.jSimDonjon.Dungeon;
import fr.jSimDonjon.Entity.Scoot;
import fr.jSimDonjon.Entity.Weak;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class PlateauFenetre extends JFrame {

    private static final long serialVersionUID = -6563585351564617603L;
    private final PlateauPanneau plateauPanneau;
    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu menuGame = new JMenu("Jeu");
    private final JMenu menuOptions = new JMenu("Options");
    private final JMenuItem exitGame = new JMenuItem("Quitter");
    private final JButton stepGame = new JButton("Étape suivante");
    private final JMenuItem addScoot = new JMenuItem("Ajouter un éclaireur");
    private final JMenuItem addWeak = new JMenuItem("Ajouter un lâche");
    private JLabel stats = null;
    private Dungeon dungeon;

    public PlateauFenetre(String title, int largeur, int hauteur, PlateauCase[][] plateauCases, final Dungeon dungeon) {
        this.dungeon = dungeon;
        setTitle(title);
        setSize(1000, 1000);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(3);
        this.plateauPanneau = new PlateauPanneau(largeur, hauteur, plateauCases);
        this.setContentPane(plateauPanneau);
        setVisible(true);
        setExtendedState(this.getExtendedState() | this.MAXIMIZED_BOTH);
        if (dungeon != null) {
            /*Menu*/
            this.menuGame.add(this.exitGame);
            this.menuOptions.add(this.addScoot);
            this.menuOptions.add(this.addWeak);
            this.menuBar.add(this.menuGame);
            this.menuBar.add(this.menuOptions);
            if (dungeon.configuration.getStepByStep()) {
                this.menuBar.add(this.stepGame);
            }

            this.stepGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    dungeon.setStepByStep(false);
                }
            });
            this.exitGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    System.exit(0);
                }
            });
            this.addScoot.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    dungeon.putCharacter(new Scoot(dungeon, dungeon.getMovableSize()));
                }
            });
            this.addWeak.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    dungeon.putCharacter(new Weak(dungeon, dungeon.getMovableSize()));
                }
            });
            this.stepGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    dungeon.setStepByStep(false);
                }
            });

            this.setJMenuBar(menuBar);
        }

    }

    public void placerPiece(PlateauPiece piece) {
        this.plateauPanneau.placerPiece(piece);
    }

    public void rafraichir() {
        String hpMob = !this.dungeon.getMonster().getHealthPoints().equalsIgnoreCase("0") ? this.dungeon.getMonster().getHealthPoints() : "Mort";
        this.plateauPanneau.repaint();
        this.menuBar.remove(stats);
        this.revalidate();
        this.stats = new JLabel("                                PV du monstre : " + hpMob + "    |    Nombre de personnages : " + dungeon.getNbCharAlive() + "    |    Tour n\u00B0 " + dungeon.getRound() + "   ");
        this.menuBar.add(stats);
        this.revalidate();
    }

    public void addImageToHashmap(Integer id, Image image) {
        this.plateauPanneau.images.put(id, image);
    }

    public void setStats() {
        String hpMob = this.dungeon.getMonster().getHealthPoints().equalsIgnoreCase("0") ? this.dungeon.getMonster().getHealthPoints() : "Mort";
        this.stats = new JLabel("                                PV du monstre : " + hpMob + "    |    Nombre de personnages : " + dungeon.getNbCharAlive() + "    |    Tour n\u00B0 " + dungeon.getRound() + "   ");
        this.menuBar.add(this.stats);
    }
}
